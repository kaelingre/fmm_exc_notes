# makefile for latex files

# choose compiler 
CC = latex 

# source files ( no .tex extension!!!)
SRC = fmm

# make all
all: tex pdf #bib

# plain latex compilation
tex:
	$(CC) $(SRC)
	$(CC) $(SRC)

# make bibliography
bib:	
	bibtex $(SRC)
	$(CC) $(SRC)
	$(CC) $(SRC)
	
# make a ps file
ps:

	$(CC) $(SRC)
	dvips -o $(SRC).ps -t a4 $(SRC).dvi

# make a pdf file
pdf:
	$(CC) $(SRC)
	dvips -Ppdf -G0  -o $(SRC).ps $(SRC).dvi
	ps2pdf -dPDFsettings=/prepress $(SRC).ps
	/bin/rm -f $(SRC).ps

# rm the tex crappy files and other twiggle files
clean:

	rm -rf *.aux *.dvi *.log *.toc *.lof *.lot *.blg *.end *~ sections/[0-9][0-9].aux sections/[0-9][0-9].log chapters/*~ *.out *.synctex.gz

# rm ps files as well
tidy:

	rm -rf *.end *.aux *.dvi *.log *.toc *.lof *.lot *~ $(SRC).ps $(SRC).pdf *.bbl *.blg sections/[0-9][0-9].aux sections/[0-9][0-9].log  chapters/*~ *.out *.synctex.gz

